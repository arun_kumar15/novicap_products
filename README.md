#### Running Guidelines####

Business logic written in Python3.
DOWNLOAD novicap_product_pricing.py and products_and_pricing.json files and to execute the business logic, run:
	python3 novicap_product_pricing.py

1. As per requirements, a JSON file is added where PRODUCTS and PRICING details are defined:
	PRODUCT: 
		Can be updated as per requirements and won't affect the functionality at all. We can add, delete or update the product any time as per the requirements.
	PRICING:
		Pricing rules which can be updated any time and new discount type can be added for products or new products can be added for the existing discount type

2. Assuming we are adding(scaning) items from Business Logic file input itself, you can add as many input as desirable(sample input are already provided).

3. For any newly defined or updated Discount Types in 'Pricing' of products_and_pricing json, we will just need to update 'calculate_discounted_total' of class 'CalculateDiscount' method and the rest of the business logic will remain same.

4. Again, all the methods are performing a single task, so if needed we can easily add new functionality to Business Logic and grow the Logic as required.
