import json


with open('products_and_pricing.json', 'r') as f:
	json_data = json.loads(f.read())
	products = json_data['Products']
	pricing_rules = json_data['Pricing']


class ScanAddItems():
	def __init__(self):
		self.cart = {}


	def scan_and_add_items(self, product_code):
		"""
		scan and check if product is available in store, if yes then add the product to cart for checkout
		"""
		if product_code in products.keys():
			if product_code in self.cart.keys():
				self.cart[product_code] += 1
			else:
				self.cart.update({product_code: 1})

		return self.cart


class CalculateTotalAmount():
	def __init__(self, cart):
		self.amount = 0
		self.cart = cart


	def get_total_amount(self):
		"""
		calculate the total amount of each item in the cart based on the pricing rules
		"""
		for item, count in self.cart.items():
			check_discount = CalculateDiscount()
			item_total_price = check_discount.check_discount_and_calculate_item_total(item, count)
			self.amount = self.amount + item_total_price

		print(self.amount)
		return self.amount
		

class CalculateDiscount():
	def __init__(self):
		self.item_price = 0


	def check_discount_and_calculate_item_total(self, item, count):
		"""
		check if the product exist in the discounted items,
		True:
			calculate the total price of item based on the discount applied
		False:
			calculate the total price based on the normal product price 
		"""
		if item in pricing_rules.keys() and count >= pricing_rules[item]['Threshold']:
			discount_type = pricing_rules[item]['DiscountType']
			price_per_item = pricing_rules[item]['PricePerItem']
			threshold_value = pricing_rules[item]['Threshold']
			self.item_price = self.calculate_discounted_total(discount_type, price_per_item, threshold_value, count)
		else:
			self.item_price = products[item]['Price'] * count

		return self.item_price


	def calculate_discounted_total(self, discount_type, price_per_item, threshold_value, count):
		"""
		check the discount type and calculate the item price
		New discount type can be added any time and only this section of the code needs to be modified when updating the pricing plans or new discount types are introduced, the rest of the code structure will remain the same
		"""
		if discount_type == 'TwoForOne':
			extra_item_count = count % (threshold_value)
			extra_item_price = extra_item_count * price_per_item
			discounted_item_count = (count - extra_item_count) / threshold_value
			discounted_item_price = discounted_item_count * price_per_item
			self.item_price = extra_item_price + discounted_item_price

		elif discount_type == 'XProduct':
			self.item_price = count * price_per_item

		return self.item_price
		

a = ScanAddItems()
add_item = a.scan_and_add_items('VOUCHER')
add_item = a.scan_and_add_items('TSHIRT')
add_item = a.scan_and_add_items('VOUCHER')
add_item = a.scan_and_add_items('VOUCHER')
add_item = a.scan_and_add_items('MUG')
add_item = a.scan_and_add_items('TSHIRT')
add_item = a.scan_and_add_items('TSHIRT')
b = CalculateTotalAmount(add_item)
amount = b.get_total_amount()
